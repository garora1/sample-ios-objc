//
//  SingletonClass.m
//  Hotlap
//
//  Created by Itesh on 11/03/16.
//  Copyright © 2017 EV/A, Inc. All rights reserved.
//


#import "SingletonClass.h"
#import "Constant.h"
#import "Hotlap-Bridging-Header.h"
#import <CoreLocation/CoreLocation.h>
#define APP_NAME @"Hotlap"
@implementation SingletonClass


#pragma mark - Shared Instance
+(instancetype)sharedInstance
{
    static SingletonClass *_sharedInstance = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedInstance = [[SingletonClass alloc] init];
    });
    return _sharedInstance;
}

#pragma mark - Itesh Misc Functions -

/**
 Resize the image according to device.
 @param image an instance of UIImage.
 @return UIImage.
 */

+(UIImage *)resizeImageForDevice:(UIImage *)image{
    UIImage *scaledImage;
    
    if ([UIScreen mainScreen].scale >2.9) {
        scaledImage = image;
    }
    else if ([UIScreen mainScreen].scale >1.9){
        scaledImage=     [UIImage imageWithCGImage:[image CGImage] scale:(image.scale * 0.66) orientation:(image.imageOrientation)];
    }
    else if ([UIScreen mainScreen].scale >0.9){
        scaledImage=     [UIImage imageWithCGImage:[image CGImage] scale:(image.scale * 0.33) orientation:(image.imageOrientation)];
    }
    return scaledImage;
}


/**
 Removes extra whitespacing by trimming characters from start and end.
 @param originalString an instance of NSString.
 @return NSString.
 */

+(NSString*)stringWithoutSpaces:(NSString*)originalString{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [originalString stringByTrimmingCharactersInSet:whitespace];
    return trimmed;
}

/**
 Change number into desired string by using NSNumberFormatter.
 @param number an instance of int.
 @return NSString.
 */
+(NSString*)getFormattedNumberStringFor:(int)number {
    
    float floatNum = (float)number;
    NSString *formattedNumber = @"";
    if(number>1000) {
        float remainder =(float)(number%1000);
        if(remainder>0)
            formattedNumber = [NSString stringWithFormat:@"%.1fK", (floatNum/1000)];
        else
            formattedNumber = [NSString stringWithFormat:@"%dK", (number/1000)];
    }
    else if(number>10000){
        float remainder =(float)(number%10000);
        
        if(remainder>0)
            formattedNumber = [NSString stringWithFormat:@"%.1fK", floatNum/10000];
        else
            formattedNumber = [NSString stringWithFormat:@"%dK", (number/10000)];
        
    }
    else if(number>1000000){
        float remainder =(float)(number%1000000);
        
        if(remainder>0)
            formattedNumber = [NSString stringWithFormat:@"%.1fM", floatNum/1000000];
        
        else
            formattedNumber = [NSString stringWithFormat:@"%dM", (number/1000000)];
    }
    else if(number>10000000){
        
        float remainder =(float)(number%10000000);
        if(remainder>0)
            formattedNumber = [NSString stringWithFormat:@"%.1fM", floatNum/10000000];
        else
            formattedNumber = [NSString stringWithFormat:@"%dM", (number/10000000)];
    }
    
    else formattedNumber = [NSString stringWithFormat:@"%d", number];
    return formattedNumber;
}

+(void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize{
    
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
    roundedView.clipsToBounds = YES;
    roundedView.layer.borderColor = [UIColor whiteColor].CGColor;
    roundedView.layer.borderWidth = 1.5;
}

/**
 Get size of string based on label font and max size of label based on its bounds.
 @param aString an instance of NSString,aSize an instance of CGSize.
 @return CGSize.
 */
+(CGSize)sizeOfTextString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize{
    
    CGSize sizeOfText = [aString boundingRectWithSize: aSize options: (NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes: [NSDictionary dictionaryWithObject:aFont forKey:NSFontAttributeName] context: nil].size;
    return sizeOfText;
}

/**
 Get date from selected string.
 @param dateString an instance of NSString.
 @return NSDate.
 */
+(NSDate *) getDateFromString:(NSString *)dateString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *date =[dateFormatter dateFromString:dateString];
    if (!date) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        date =[dateFormatter dateFromString:dateString];
    }
    return date;
}

/**
 Get string from date.
 @param selectedDate an instance of NSDate.
 @return NSString.
 */
+(NSString *) getStringFromDate:(NSDate *)selectedDate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
    return [dateFormatter stringFromDate:selectedDate];
}

/**
 Validate email using a regex expression
 @param strEmail an instance of NSString.
 @return Bool true if it is valid email id.
 */

+(BOOL)validateEmail:(NSString*)strEmail{
    
    NSString *emailRegex= @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    BOOL valid=[emailTest evaluateWithObject:strEmail];
    return valid;
}

/**
 Add padding view on left hand side of text field
 @param textField an instance of UITextField.
 */

+(void) paddingTextField:(UITextField *) textField{
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

/**
 Add padding view on left hand side of text field
 @param textField an instance of UITextField.
 */
+(void) paddingTextFieldInSearchController:(UITextField *) textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

/**
 Resize and return image with correct orientation which will be used while sending image file to server.
 @param inImage an instance of UIImage,thumbRect an instance of CGRect.
 @return an instance of UIImage.
 */
+(UIImage* )resizedImage:(UIImage *)inImage rect:(CGRect) thumbRect{
    
    CGImageRef	imageRef = [inImage CGImage];
    CGImageAlphaInfo	alphaInfo = CGImageGetAlphaInfo(imageRef);
    // There's a wierdness with kCGImageAlphaNone and CGBitmapContextCreate
    // see Supported Pixel Formats in the Quartz 2D Programming Guide
    // Creating a Bitmap Graphics Context section
    // only RGB 8 bit images with alpha of kCGImageAlphaNoneSkipFirst, kCGImageAlphaNoneSkipLast, kCGImageAlphaPremultipliedFirst,
    // and kCGImageAlphaPremultipliedLast, with a few other oddball image kinds are supported
    // The images on input here are likely to be png or jpeg files
    if (alphaInfo == kCGImageAlphaNone)
        alphaInfo = kCGImageAlphaNoneSkipLast;
    
    // Build a bitmap context that's the size of the thumbRect
    CGContextRef bitmap = CGBitmapContextCreate(
                                                NULL,
                                                thumbRect.size.width,	// width
                                                thumbRect.size.height,	// height
                                                CGImageGetBitsPerComponent(imageRef),	// really needs to always be 8
                                                4 * thumbRect.size.width,	// rowbytes
                                                CGImageGetColorSpace(imageRef),
                                                alphaInfo
                                                );
    
    // Draw into the context, this scales the image
    CGContextDrawImage(bitmap, thumbRect, imageRef);
    
    // Get an image from the context and a UIImage
    CGImageRef	ref = CGBitmapContextCreateImage(bitmap);
    UIImage*	result = [UIImage imageWithCGImage:ref];
    CGContextRelease(bitmap);	// ok if NULL
    CGImageRelease(ref);
    return result;
}


+ (UIImage*)resizeImage:(UIImage*)image withWidth:(CGFloat)width withHeight:(CGFloat)height
{
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width/image.size.width;
    CGFloat heightRatio = newSize.height/image.size.height;
    if(widthRatio > heightRatio){
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    }
    else{
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(void)setGridImage:(UIImage*)image forButton:(UIButton *)button {
    
    UIImageView *gridImageView;
    gridImageView = (UIImageView*)[button viewWithTag:1];
    if(!gridImageView){
        gridImageView = [[UIImageView alloc]init];
        gridImageView.frame = CGRectMake(5, 5, button.frame.size.width-11, button.frame.size.height-11);
        [button addSubview:gridImageView];
        gridImageView.tag = 1;
    }
    gridImageView.hidden = NO;
    gridImageView.image = image;
    
    UIImageView *placeholderImageView;
    placeholderImageView = (UIImageView*)[button viewWithTag:2];
    
    if(!placeholderImageView){
        placeholderImageView = [[UIImageView alloc]init];
        placeholderImageView.frame = CGRectMake(0, 0, button.frame.size.width, button.frame.size.height);
        [button addSubview:placeholderImageView];
        [button sendSubviewToBack:placeholderImageView];
        placeholderImageView.tag = 2;
        placeholderImageView.image = [UIImage imageNamed:@"thumbnail_placeholder.png"];
    }
    placeholderImageView.hidden = NO;
    UIImageView *playImageView;
    playImageView = (UIImageView*)[button viewWithTag:23];
    
    if(!playImageView){
        playImageView = [[UIImageView alloc]init];
        playImageView.frame = CGRectMake(25, 25, button.frame.size.width/2, button.frame.size.height/2);
        [button addSubview:playImageView];
        playImageView.tag = 23;
        playImageView.image = [UIImage imageNamed:@"feed_play_watermark.png"];
        playImageView.hidden = YES;
    }
}

+ (UIView *)loadViewFromNib:(NSString *)nibName forClass:(id)forClass{
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    for(id currentObject in topLevelObjects)
        if([currentObject isKindOfClass:forClass]){
            return currentObject ;
        }
    return nil;
}

+ (NSString *) getCurrentDate{
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"EEE, MMM dd, yyyy"];
    NSString *dateStr = [df stringFromDate:todayDate];
    return dateStr;
}

/**
 Check whether selected string is valid email address or not
 @param emailString an instance of String
 @return Bool instance
 */

+(BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    if (regExMatches == 0) {
        return NO;
    }
    else{
        return YES;
    }
}

/**
 Check whether selected object is kind of class NSNull
 @param data an instance of NSobject
 @return Bool instance
 */
+(BOOL) validData:(id) data
{
    if(data!=nil){
        if(![data isKindOfClass:[NSNull class]]){
            return YES;
        }
    }
    return NO;
}

/**
 Check whether selected string is empty
 @param string an instance of NSString
 @return Bool instance
 */
+(BOOL) emptyString:(NSString*)string{
    return ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length] == 0);
}

/**
 Get difference between 2 dates
 @param fromDateTime an instance of NSDate,toDateTime an instance of NSDate
 @return NSInteger instance
 */
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay fromDate:fromDate toDate:toDate options:0];
    return [difference second];
}

/**
 Get font size according to device
 @param size an instance of CGFloat
 */
+(CGFloat)getFontSizeAccordingToDevice:(CGFloat)size{
    return size*[SingletonClass getScaleFactorForiPhoneDevice];
}

/**
 Get color based on hexString
 @param hexString an instance of NSString
 @return an instance of UIColor
 */
+ (UIColor *) colorWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

/**
 Get formatted string based on selectedAmountNum
 @param hexString an instance of NSString
 @return an instance of NSString
 */

+(NSString *)getFormattedString:(NSNumber *)selectedAmountNum{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setGroupingSeparator:@","];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMinimumIntegerDigits:1];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:selectedAmountNum];
    return formattedNumberString;
    
}
+(NSString *)getJsonObjectFromSelectedArray:(NSArray *)object{
    
    NSError *error = nil;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonString);
    return jsonString;
}

+(CGFloat) getScaleFactorForiPhoneDevice{
    
    if (IS_IPHONE_6_PLUS_CONSTANT) {
        return 1.29;
    }
    else if (IS_IPHONE_6_CONSTANT) {
        return 1.17;
    }
    return 1;
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) maxValue{
    
    if (sourceImage.size.width>maxValue || sourceImage.size.height>maxValue) {
        if (sourceImage.size.width>sourceImage.size.height) {
            float oldWidth = sourceImage.size.width;
            float scaleFactor = maxValue / oldWidth;
            
            float newHeight = sourceImage.size.height * scaleFactor;
            float newWidth = oldWidth * scaleFactor;
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return newImage;
        }
        else{
            float oldHeight = sourceImage.size.height;
            float scaleFactor = maxValue / oldHeight;
            
            float newHeight = oldHeight * scaleFactor;
            float newWidth = sourceImage.size.width * scaleFactor;
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return newImage;
        }
    }
    else{
        return sourceImage;
    }
}

+(BOOL)isDefaultLanguageEnglish{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language rangeOfString:@"es-US"].location != NSNotFound
        || [language isEqualToString:@"es"] ) {
        return NO;
    }
    return YES;
}

+(BOOL)validateString:(NSString *)objectString{
    
    if (objectString && ![objectString isEqual:[NSNull null]] && objectString.length) {
        return YES;
    }
    return NO;
}

+(NSString*)getStringFromDate:(NSDate*)date type:(int)type {
    
    NSDate *currentDate = [NSDate date];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    float timeZoneOffset = [destinationTimeZone secondsFromGMTForDate:currentDate] ;
    currentDate = [currentDate dateByAddingTimeInterval:timeZoneOffset];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:date];
    if (timeInterval<0) {
        timeInterval=-timeInterval;
    }
    
    if(timeInterval>=86400) {
        int days = (timeInterval/86400);
        if (days>365) {
            int years = days/365;
            switch (type) {
                case 1:
                    return [NSString stringWithFormat:@"%d Y",years];
                    break;
                    
                default:
                    return [NSString stringWithFormat:@"%d year ago",years];
                    break;
            }
        }
        
        else if (days>31) {
            int months = days/30;
            switch (type) {
                case 1:
                    return [NSString stringWithFormat:@"%d M",months];
                    break;
                default:
                    return [NSString stringWithFormat:@"%d months ago",months];
                    break;
            }
        }
        else if (days>7) {
            int weeks = days/7;
            switch (type) {
                case 1:
                    return [NSString stringWithFormat:@"%d W",weeks];
                    break;
                default:
                    if (weeks > 0) {
                        return [NSString stringWithFormat:@"%d weeks ago",weeks];
                    }
                    else{
                        return [NSString stringWithFormat:@"%d week ago",weeks];
                    }
                    break;
            }
        }
        else{
            switch (type) {
                case 1:
                    return [NSString stringWithFormat:@"%d d",days];
                    break;
                    
                default:
                    return [NSString stringWithFormat:@"%d days ago",days];
                    break;
            }
        }
    }
    else if(timeInterval>=3600) {
        int hours = (timeInterval/3600);
        switch (type) {
            case 1:
                return [NSString stringWithFormat:@"%d h",hours];
                break;
            default:
                return [NSString stringWithFormat:@"%d hours ago",hours];
                break;
        }
    }
    else if(timeInterval>=60) {
        
        int minutes = timeInterval/60;
        switch (type) {
            case 1:
                return [NSString stringWithFormat:@"%d m",minutes];
                break;
            default:
                return [NSString stringWithFormat:@"%d minutes ago",minutes];
                break;
        }
    }
    else {
        int seconds = timeInterval;
        if(seconds<0)
            seconds = 0;
        switch (type) {
            case 1:
                return [NSString stringWithFormat:@"%d s",seconds];
                /* Not required as of now
                 if (seconds>=5) {
                 return [NSString stringWithFormat:@"%d s",seconds];
                 }
                 else
                 return [NSString stringWithFormat:@"Just Now"];
                 */
                break;
            default:
                return [NSString stringWithFormat:@"%d seconds ago",seconds];
                break;
        }
    }
}

+(NSString *)getTimeDifferenceBetweenDates:(NSString *)postDateString{
    NSString *difference = nil;
    
    NSDate *postDate = [SingletonClass getDateFromString:postDateString];
    if (!postDate) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        postDate =  [dateFormatter dateFromString:postDateString];
    }
    if (!postDate) {
        return @"";
    }
    difference= [SingletonClass getStringFromDate:postDate type:1];
    return difference;
}

+(NSString *)removeExtraCharacters:(NSString *)textString{
    NSString *originalString = textString;
    originalString  = [originalString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    originalString = [originalString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    originalString  = [originalString stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    return originalString;
}

#pragma mark - Capture Image and Video

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


+ (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution;//
    // Future use
    //    if (SCREEN_WIDTH*[[UIScreen mainScreen]nativeScale]>1200) {
    //        kMaxResolution =  1200;
    //    }
    //    else{
    //        kMaxResolution =  SCREEN_WIDTH*[[UIScreen mainScreen]nativeScale];
    //
    //    }
    kMaxResolution =  SCREEN_WIDTH*[[UIScreen mainScreen]nativeScale];
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else{
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageCopy;
}



+ (UIImage *)convertImageToGrayScale:(UIImage *)image {
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    // Return the new grayscale image
    return newImage;
    
}



+ (UIImage *)resizeImageIntoSquareImageBasedOnSelectedImage:(UIImage *)image{
    
    CGSize newSize;
    CGFloat scaleRatio;
    
    if (image.size.width > image.size.height) {
        scaleRatio = image.size.height/image.size.width;
        newSize = CGSizeMake(image.size.height, image.size.height);
    }else{
        scaleRatio = image.size.width/image.size.height;
        newSize = CGSizeMake(image.size.width, image.size.width);
    }
    //avoid redundant drawing
    if (CGSizeEqualToSize(image.size, newSize)){
        return image;
    }
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0f);
    
    //draw
    [image drawInRect:CGRectMake(0.0f, 0.0f, newSize.width, newSize.height)];
    //capture resultant image
    UIImage *newImage1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage1;
}

+(CGFloat)calculateDistanceBetweenTwoLocationWithSourceLat1:(double)sourceLatitude sourceLong:(double)sourceLongitude destinationLatutude:(double)destinationLatitude destinationLongitude:(double)destinationLongitude{
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:sourceLatitude longitude:sourceLongitude];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:destinationLatitude longitude:destinationLongitude];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    return distance;
    
    //    return distance/1000;
}

@end
