//
//  SingletonClass.h
//  Hotlap
//
//  Created by Itesh Dutt on 11/03/16.
//  Copyright © 2017 EV/A, Inc. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFNetworking.h"

typedef void (^completionBlock) (BOOL succeeded, id resultResponse, id responseObject);
typedef void (^failureBlock) (BOOL succeeded, id failureResponse,NSURLResponse *response,NSString *error_desc);

@interface SingletonClass : NSObject{
    AFURLSessionManager *manager;
    
}
@property (strong,nonatomic) NSMutableDictionary *dict_SignUpParameters;
@property (strong) NSString *str_DeviceToken;

+(instancetype)sharedInstance;

//Itesh new functions
+(NSString*)stringWithoutSpaces:(NSString*)originalString;
+(BOOL)validateEmail:(NSString*)strEmail;
+ (UIImage*)resizeImage:(UIImage*)image withWidth:(CGFloat)width withHeight:(CGFloat)height;
+(void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize;
+ (UIColor *) colorWithHexString: (NSString *) hexString;
+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;
+(CGSize)sizeOfTextString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize;
+(NSString*)getFormattedNumberStringFor:(int)number ;
+(UIImage *)resizeImageForDevice:(UIImage *)image;
+(NSDate *) getDateFromString:(NSString *)dateString;
+(NSString *) getStringFromDate:(NSDate *)selectedDate;
+(void) paddingTextField:(UITextField *) textField;
+(void) paddingTextFieldInSearchController:(UITextField *) textField;
+(UIImage* )resizedImage:(UIImage *)inImage rect:(CGRect) thumbRect;
+(void)setGridImage:(UIImage*)image forButton:(UIButton *)button ;
+ (UIView *)loadViewFromNib:(NSString *)nibName forClass:(id)forClass;
+ (NSString *) getCurrentDate;
+(BOOL) validEmail:(NSString*) emailString ;
+(BOOL) validData:(id) _data;
+(BOOL) emptyString:(NSString*)string;
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+(CGFloat)getMaximumWidth:(CGFloat)min_width forKey:(NSString *)key_value forSelectedArray:(NSArray *)selectedArray withFont:(UIFont *)font withMaxSize:(CGSize)selectedSize;
+(NSString *)getFormattedString:(NSNumber *)selected_amount_num;
+(CGFloat) getScaleFactorForiPhoneDevice;
+(BOOL)isDefaultLanguageEnglish;
+(NSString*)getStringFromDate:(NSDate*)date type:(int)type;
+(NSString *)getTimeDifferenceBetweenDates:(NSString *)post_date_string;
+(NSString *)removeExtraCharacters:(NSString *)text_string;
+ (UIImage *)imageFromColor:(UIColor *)color ;
+ (UIImage *)resizeImageIntoSquareImageBasedOnSelectedImage:(UIImage *)image ;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;

+(CGFloat)calculateDistanceBetweenTwoLocationWithSourceLat1:(double)sourceLatitude sourceLong:(double)sourceLongitude destinationLatutude:(double)destinationLatitude destinationLongitude:(double)destinationLongitude;
+(CGFloat)getFontSizeAccordingToDevice:(CGFloat)size;
+(NSString *)getJsonObjectFromSelectedArray:(NSArray *)object;
+ (UIImage *)convertImageToGrayScale:(UIImage *)image ;
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) maxValue;
@end

